# copyright: yueshi@usc.edu
import pandas as pd 
import hashlib
import os 
from utils import logger
def file_as_bytes(file):
    with file:
        return file.read()

def extractMatrix(dirname):
	'''
	return a dataframe of the miRNA matrix, each row is the miRNA counts for a file_id

	'''
	count = 0

	miRNA_data = []
	for idname in os.listdir(dirname):
		# list all the ids 
		if idname.find("-") != -1:
			idpath = dirname +"/" + idname

			# all the files in each id directory
			for filename in os.listdir(idpath):
				# check the miRNA file
				if filename.find("-") != -1:

					filepath = idpath + "/" + filename
					df = pd.read_csv(filepath,sep="\t")
					# columns = ["miRNA_ID", "read_count"]
					if count ==0:
						# get the miRNA_IDs 
						miRNA_IDs = df.miRNA_ID.values.tolist()

					id_miRNA_read_counts = [idname] + df.read_count.values.tolist()
					miRNA_data.append(id_miRNA_read_counts)


					count +=1
					# print (df)
	columns = ["file_id"] + miRNA_IDs
	df = pd.DataFrame(miRNA_data, columns=columns)
	return df


if __name__ == '__main__':


	data_dir ="/Users/Shubham/Desktop/USC_Data/Sem3courses/EE542/lab10/data_full/"
	# Input directory and label file. The directory that holds the data. Modify this when use.
	dirname = data_dir + "livemirna"
	label_file = data_dir + "files_meta.tsv"
	
	#output file
	outputfile = data_dir + "half_matrix.csv"

	# extract data
	matrix_df = extractMatrix(dirname)
	

	#merge the two based on the file_id
	result = matrix_df
	#print(result)

	#save data
	result.to_csv(outputfile, index=False)
	#print (labeldf)

 




