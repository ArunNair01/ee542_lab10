# EE542_Lab10

##Install all the packages needed. (sklearn, pandas)

pip install sklearn

pip install pandas

Pip install matplotlib


##Source codes:

**check.py**:

		 This is to check the integrity for the downloaded miRNA files
		 python check.py.  

**parse_file_case_id.py**:  

		This is to get the unique file id and the corresponding case ids.	
		python parse_file_case_id.py

**request_meta.py**: 

	This is to request the meta data for the files and cases.
		python request_meta.py

**gen_miRNA_matrix.py**: 

		This is to generate half of the miRNA matrix(half_matrix) for all the files 
		python gen_miRNA_matrix.py

**dominators_label.py**: 

	This is to generate the labels for all the files and then merge the matrix created in previous step with this, and finally generate the miRNA matrix.
		python dominators_label.py

**predict.py** : 

	This is for applying Decision Tree Machine learning algorithm to the miRNA matrix for all cancer types detection.
		python predict.py

