# copyright: TEAM_DOMINATORS
import pandas as pd 
import hashlib
import os 
from utils import logger
import pdb
## Cnacer types based on primarty sites

##No cancer - 0
##Kidney cancer - 1
##Thyroid cancer - 2
##Blood cancer - 3
##Uterus cancer - 4
##Head and Neck cancer - 5
##Ovary cancer - 6
##Breast cancer - 7
##Liver cancer - 8
##Esophagus cancer - 9
##Brain cancer - 10
##Cervix cancer - 11
##Lung cancer - 12
##Bladder cancer - 13
##Colorectal cancer - 14
##Stomach cancer - 15
##Eye cancer -16
##Pancreas cancer - 17
##Adrenal Gland cancer - 18
##Prostate cancer - 19
##Soft Tissue cancer - 20
##Testis cancer - 21
##Skin cancer - 22
##Bone Marrow cancer - 23
##Lymph Nodes cancer - 24
##Pleura cancer - 25
##Thymus cancer - 26
##Bile Duct cancer - 27




def extractLabel(inputfile):
    
            df = pd.read_csv(inputfile, sep="\t")
            #
            # print (df[columns])
            
            
            
            df['label'] = df['cases.0.project.primary_site']
            
            df.loc[df['cases.0.project.primary_site'].str.match("Kidney"), 'label'] = 1
            df.loc[df['cases.0.project.primary_site'].str.match("Thyroid"), 'label'] = 2
            df.loc[df['cases.0.project.primary_site'].str.match("Blood"), 'label'] = 3
            df.loc[df['cases.0.project.primary_site'].str.match("Uterus"), 'label'] = 4
            df.loc[df['cases.0.project.primary_site'].str.match("Head and Neck"), 'label'] = 5
            df.loc[df['cases.0.project.primary_site'].str.match("Ovary"), 'label'] = 6
            df.loc[df['cases.0.project.primary_site'].str.match("Breast"), 'label'] = 7
            df.loc[df['cases.0.project.primary_site'].str.match("Liver"), 'label'] = 8
            df.loc[df['cases.0.project.primary_site'].str.match("Esophagus"), 'label'] = 9
            df.loc[df['cases.0.project.primary_site'].str.match("Brain"), 'label'] = 10
            df.loc[df['cases.0.project.primary_site'].str.match("Cervix"), 'label'] = 11
            df.loc[df['cases.0.project.primary_site'].str.match("Lung"), 'label'] = 12
            df.loc[df['cases.0.project.primary_site'].str.match("Bladder"), 'label'] = 13
            df.loc[df['cases.0.project.primary_site'].str.match("Colorectal"), 'label'] = 14
            df.loc[df['cases.0.project.primary_site'].str.match("Stomach"), 'label'] = 15
            df.loc[df['cases.0.project.primary_site'].str.match("Eye"), 'label'] = 16
            df.loc[df['cases.0.project.primary_site'].str.match("Pancreas"), 'label'] = 17
            df.loc[df['cases.0.project.primary_site'].str.match("Adrenal"), 'label'] = 18
            df.loc[df['cases.0.project.primary_site'].str.match("Prostate"), 'label'] = 19
            df.loc[df['cases.0.project.primary_site'].str.match("Soft Tissue"), 'label'] = 20
            df.loc[df['cases.0.project.primary_site'].str.match("Testis"), 'label'] = 21
            df.loc[df['cases.0.project.primary_site'].str.match("Skin"), 'label'] = 22
            df.loc[df['cases.0.project.primary_site'].str.match("Bone Marrow"), 'label'] = 23
            df.loc[df['cases.0.project.primary_site'].str.match("Lymph Nodes"), 'label'] = 24
            df.loc[df['cases.0.project.primary_site'].str.match("Pleura"), 'label'] = 25
            df.loc[df['cases.0.project.primary_site'].str.match("Thymus"), 'label'] = 26
            df.loc[df['cases.0.project.primary_site'].str.match("Bile Duct"), 'label'] = 27
            df.loc[df['cases.0.samples.0.sample_type'].str.match("Solid Tissue Normal"), 'label'] = 0
            
            
            
            
	
            #primary_tumor_count = df.loc[df.label == 1].shape[0]
           
            #logger.info("{} Primary Tumor samples, {} Recurrent Tumor,{} Solid Tissue Normal,{} Peripheral Blood,{} Additional Metastatic,{} Primary Blood Derived Cancer - Bone Marrow,{} Recurrent Blood Derived Cancer - Bone Marrow,{} Recurrent Blood Derived Cancer - Peripheral Blood,{} Additional - New Primary,{} Cell Lines, {} Control Analyte, {} Metastatic  ".format(primary_tumor_count,recurrent_tumor_count,tissue_count,peripheral_blood_count,Additional_metastatic_count,Primary_Blood_Derived_Cancer_Bone_Marrow_count,Recurrent_Blood_Derived_Cancer_Bone_Marrow_count,Recurrent_Blood_Derived_Cancer_Peripheral_Blood_count,Additional_New_Primary_count,Cell_Lines_count,Control_Analyte_count,Metastatic_count))
            columns = ['file_id','label']
            return df[columns]

if __name__ == '__main__':


        data_dir ="/Users/Shubham/Desktop/USC_Data/Sem3courses/EE542/lab10/data_full/"
        # Input directory and label file. The directory that holds the data. Modify this when use.
        
        label_file = data_dir + "files_meta.tsv"
        
        #output file
        outputfile = data_dir + "miRNA_matrix.csv"

        # extract data
        label_df = extractLabel(label_file)

        input_half=data_dir+"half_matrix.csv"
        matrix_df=pd.read_csv(input_half)

        #merge the two based on the file_id
        result = pd.merge(matrix_df, label_df, on='file_id', how="left")
        #print(result)

        #save data
        result.to_csv(outputfile, index=False)
        #print (labeldf)
