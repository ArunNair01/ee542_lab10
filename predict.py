# copyright: yueshi@usc.edu
import pandas as pd 
import hashlib
import os 
import logging
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import numpy as np
import pdb
import matplotlib.pyplot as plt
#from ggplot import *
from sklearn import datasets
from sklearn.metrics import roc_curve,auc
from scipy import interp
from itertools import cycle
import time

from sklearn.manifold import TSNE
from sklearn.feature_selection import SelectFromModel
from sklearn import datasets
from sklearn.linear_model import LassoCV
from sklearn.linear_model import MultiTaskLassoCV
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA


#def lassoSelection(X,y,)
#'Bile Duct cancer'
label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer','Bile Duct cancer']
def lassoSelection(X_train, y_train, n):
	'''
	Lasso feature selection.  Select n features. 
	'''
	#lasso feature selection
	#print (X_train)
	clf = LassoCV()
	sfm = SelectFromModel(clf, threshold=0)
	sfm.fit(X_train, y_train)
	X_transform = sfm.transform(X_train)
	n_features = X_transform.shape[1]
	
	#print(n_features)
	while n_features > n:
		sfm.threshold += 0.01
		X_transform = sfm.transform(X_train)
		n_features = X_transform.shape[1]
	features = [index for index,value in enumerate(sfm.get_support()) if value == True  ]
	logging.info("selected features are {}".format(features))
	return features


#def specificity_score(y_true, y_predict):
def specificity_score(TN,FP):
	'''
	true_negative rate
	'''
	#true_negative = len([index for index,pair in enumerate(zip(y_true,y_predict)) if pair[0]==pair[1] and pair[0]==0 ])
	#real_negative = len(y_true) - sum(y_true)
	#return true_negative / real_negative
	TNR = TN/(TN+FP)
	return TNR

def plot_roc(pred1,y_test):
        
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        n_classes=27
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(np.array(pd.get_dummies(y_test))[:, i], np.array(pd.get_dummies(pred1))[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])
        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr[i], tpr[i])
        mean_tpr /= n_classes
        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
        lw=2
        plt.figure(figsize=(8,5))
        plt.plot(fpr["macro"], tpr["macro"],
                 label='macro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["macro"]),
                 color='green', linestyle=':', linewidth=4)

        colors = cycle(['aqua', 'darkorange', 'cornflowerblue','green','purple','yellow','black','bisque','maroon','indianred','darkgoldenrod','honeydew','skyblue','mintcream','slategrey','oldlace','floralwhite','aqua','fuchsia','violet','gold','ivory','darksalmon','chocolate','lawngreen','tan','lime','lightsteelblue'])
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                     label='ROC curve of class {0} (area = {1:0.2f})'
                     ''.format(label_list[i], roc_auc[i]))

        plt.plot([0, 1], [0, 1], 'k--',color='red', lw=lw)
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.annotate('Random Guess',(.5,.48),color='red')
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver Operating Characteristic for decisiontree')
        plt.legend(loc="lower right")
        plt.draw()
        plt.pause(0.001)
        input("Press [enter] to continue.")
        #pdb.set_trace()

def model_fit_predict(X_train,X_test,y_train,y_test):

        np.random.seed(2018)
        
        from sklearn.tree import DecisionTreeClassifier
        from sklearn.linear_model import LogisticRegression
        from sklearn.ensemble import RandomForestClassifier
        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.ensemble import GradientBoostingClassifier
        from sklearn.ensemble import ExtraTreesClassifier
        from sklearn.svm import SVC
        from sklearn.metrics import precision_score
        from sklearn.metrics import accuracy_score
        from sklearn.metrics import f1_score
        from sklearn.metrics import recall_score
        from sklearn.metrics import confusion_matrix
        models = {
                'LogisticRegression': LogisticRegression(),
                'ExtraTreesClassifier': ExtraTreesClassifier(),
                'RandomForestClassifier': RandomForestClassifier(),
        'AdaBoostClassifier': AdaBoostClassifier(),
        'GradientBoostingClassifier': GradientBoostingClassifier(),
        'SVC': SVC(),
        'DecisionTreeClassifier':DecisionTreeClassifier()
        }
        tuned_parameters = {
                'LogisticRegression':{'C': [1, 10]},
                'ExtraTreesClassifier': { 'n_estimators': [16, 32] },
                'RandomForestClassifier': { 'n_estimators': [16, 32] },
        'AdaBoostClassifier': { 'n_estimators': [16, 32] },
        'GradientBoostingClassifier': { 'n_estimators': [16, 32], 'learning_rate': [0.8, 1.0] },
        'SVC': {'kernel': ['rbf'], 'C': [1, 10], 'gamma': [0.001, 0.0001]},
        'DecisionTreeClassifier': {}
        }
        scores= {}
        #for key in models:
       
        clf = GridSearchCV(models['DecisionTreeClassifier'], tuned_parameters['DecisionTreeClassifier'],scoring=None,  refit=True, cv=10)
        clf.fit(X_train,y_train)
        y_test_predict = clf.predict(X_test)
        cnf_matrix = confusion_matrix(y_test, y_test_predict)
        FP = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)  
        FN = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
        TP = np.diag(cnf_matrix)
        TN = cnf_matrix.sum() - (FP + FN + TP)

        FP = FP.astype(float)
        FN = FN.astype(float)
        TP = TP.astype(float)
        TN = TN.astype(float)

        TPR = TP/(TP+FN)
        TNR = TN/(TN+FP)
        FPR = FP/(FP+TN)
        FNR = FN/(TP+FN)

        PPV = TP/(TP+FP)
        ACC = (TP+TN)/(TP+FP+FN+TN)
        recall = TP/(TP+FN)
        #print(PPV,ACC,recall)
        #pdb.set_trace()
        plot_roc(y_test_predict,y_test)
        #pdb.set_trace()
        precision = precision_score(y_test, y_test_predict,average=None)
        #accuracy = accuracy_score(y_test, y_test_predict)
        accuracy=ACC
        f1 = f1_score(y_test, y_test_predict,average=None)
        recall = recall_score(y_test, y_test_predict,average=None)
        #specificity = specificity_score(y_test, y_test_predict)
        specificity = specificity_score(TN,FP)
        scores['DecisionTreeClassifier'] = [precision,accuracy,f1,recall,specificity]
        #print(cnf_matrix)
        #print(scores)
        #pdb.set_trace()

        return scores

def PCA_visual(data,df1):
    
    pca = PCA(n_components=4)
    pca_result = pca.fit_transform(data)
    principalDf = pd.DataFrame(data = pca_result
                               , columns = ['principal component 1', 'principal component 2', 'principal component 3','principal component 4'])
    finalDf = pd.concat([principalDf, df1[['label']]], axis = 1)
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('2 component PCA', fontsize = 20)
    label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer' ,'Bile Duct cancer']
    targets=[]
    for i in range(0,28):
        targets.append(i)
    #targets = ['Adrenal Gland cancer','Bile Duct cancer','Bladder cancer','Blood cancer','Bone Marrow cancer','Brain cancer','Breast cancer','Cervix cancer','Colorectal cancer','Esophagus cancer','Eye cancer','Head and Neck cancer','Kidney cancer','Liver cancer','Lung cancer','Lymph Nodes cancer','No cancer','Ovary cancer','Pancreas cancer','Pleura cancer','Prostate cancer','Skin cancer','Soft Tissue cancer','Stomach cancer','Testis cancer','Thymus cancer','Thyroid cancer','Uterus cancer']
    colors = ['r', 'g', 'b','y','khaki','m','black','bisque','maroon','indianred','darkgoldenrod','honeydew','skyblue','mintcream','slategrey','oldlace','deeppink','aqua','fuchsia','violet','gold','ivory','darksalmon','chocolate','lawngreen','tan','lime','darkcyan']
    for target, color in zip(targets,colors):
        indicesToKeep = finalDf['label'] == target
        ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                   , finalDf.loc[indicesToKeep, 'principal component 2']
                   , c = color
                   , s = 50)
    ax.legend(label_list)
    ax.grid()
    #plt.show()
    plt.draw()
    plt.pause(0.001)
    input("Press [enter] to continue.")


def tsne_visual(data,df1,pca=False):

    if(pca == True):
        pca_50 = PCA(n_components=50)
        pca_result_50 = pca_50.fit_transform(data)
        tsne = TSNE(n_components=2)
        tsne_result = tsne.fit_transform(pca_result_50)
        principalDf = pd.DataFrame(data = tsne_result
                 , columns = ['principal component 1', 'principal component 2'])
        finalDf = pd.concat([principalDf, df1[['label']]], axis = 1)
        fig = plt.figure(figsize = (8,8))
        ax = fig.add_subplot(1,1,1) 
        ax.set_xlabel('x_tsne-pca', fontsize = 15)
        ax.set_ylabel('y_tsne-pca', fontsize = 15)
        ax.set_title('tsne dimensions', fontsize = 20)

        label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer' ,'Bile Duct cancer']
        targets=[]
        for i in range(0,28):
            targets.append(i)
        #targets = ['Adrenal Gland cancer','Bile Duct cancer','Bladder cancer','Blood cancer','Bone Marrow cancer','Brain cancer','Breast cancer','Cervix cancer','Colorectal cancer','Esophagus cancer','Eye cancer','Head and Neck cancer','Kidney cancer','Liver cancer','Lung cancer','Lymph Nodes cancer','No cancer','Ovary cancer','Pancreas cancer','Pleura cancer','Prostate cancer','Skin cancer','Soft Tissue cancer','Stomach cancer','Testis cancer','Thymus cancer','Thyroid cancer','Uterus cancer']
        colors = ['r', 'g', 'b','y','khaki','m','black','bisque','maroon','indianred','darkgoldenrod','honeydew','skyblue','mintcream','slategrey','oldlace','deeppink','aqua','fuchsia','violet','gold','ivory','darksalmon','chocolate','lawngreen','tan','lime','darkcyan']
        for target, color in zip(targets,colors):
            indicesToKeep = finalDf['label'] == target
            ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                       , finalDf.loc[indicesToKeep, 'principal component 2']
                       , c = color
                       , s = 50)
        ax.legend(label_list)
        ax.grid()
        #plt.show()
        plt.draw()
        plt.pause(0.001)
        input("Press [enter] to continue.")

    else:
        
        tsne = TSNE(n_components=2)
        tsne_result = tsne.fit_transform(data)
        principalDf = pd.DataFrame(data = tsne_result
                 , columns = ['principal component 1', 'principal component 2'])
        finalDf = pd.concat([principalDf, df1[['label']]], axis = 1)
        fig = plt.figure(figsize = (8,8))
        ax = fig.add_subplot(1,1,1) 
        ax.set_xlabel('x_tsne', fontsize = 15)
        ax.set_ylabel('y_tsne', fontsize = 15)
        ax.set_title('tsne dimensions', fontsize = 20)

        label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer' ,'Bile Duct cancer']
        targets=[]
        for i in range(0,28):
            targets.append(i)
        #targets = ['Adrenal Gland cancer','Bile Duct cancer','Bladder cancer','Blood cancer','Bone Marrow cancer','Brain cancer','Breast cancer','Cervix cancer','Colorectal cancer','Esophagus cancer','Eye cancer','Head and Neck cancer','Kidney cancer','Liver cancer','Lung cancer','Lymph Nodes cancer','No cancer','Ovary cancer','Pancreas cancer','Pleura cancer','Prostate cancer','Skin cancer','Soft Tissue cancer','Stomach cancer','Testis cancer','Thymus cancer','Thyroid cancer','Uterus cancer']
        colors = ['r', 'g', 'b','y','khaki','m','black','bisque','maroon','indianred','darkgoldenrod','honeydew','skyblue','mintcream','slategrey','oldlace','deeppink','aqua','fuchsia','violet','gold','ivory','darksalmon','chocolate','lawngreen','tan','lime','darkcyan']
        for target, color in zip(targets,colors):
            indicesToKeep = finalDf['label'] == target
            ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                       , finalDf.loc[indicesToKeep, 'principal component 2']
                       , c = color
                       , s = 50)
        ax.legend(label_list)
        ax.grid()
        plt.draw()
        plt.pause(0.001)
        input("Press [enter] to continue.")

def draw(scores):
        '''
        draw scores.
        '''
        import matplotlib.pyplot as plt
        logging.info("scores are {}".format(scores))
        fig = plt.figure()
        
        
        precisions = []
        accuracies =[]
        f1_scores = []
        recalls = []
        categories = []
        specificities = []
        N = len(scores)
        ind = np.arange(N)  # set the x locations for the groups
        width = 0.01        # the width of the bars
        precisions.append(scores['DecisionTreeClassifier'][0])
        accuracies.append(scores['DecisionTreeClassifier'][1])
        f1_scores.append(scores['DecisionTreeClassifier'][2])
        recalls.append(scores['DecisionTreeClassifier'][3])
        specificities.append(scores['DecisionTreeClassifier'][4])
        i=0
        j=1
        #print(precisions)
        #print(accuracies)
        #print(f1_scores)
        #print(recalls)
        
        while(i<27):
                ax = plt.subplot(6,6,j)
                precision_bar = ax.bar(ind, precisions[0][i],width=0.01,color='b',align='center')
                accuracy_bar = ax.bar(ind+1*width, accuracies[0][i],width=0.01,color='g',align='center')
                f1_bar = ax.bar(ind+2*width, f1_scores[0][i],width=0.01,color='r',align='center')
                recall_bar = ax.bar(ind+3*width, recalls[0][i],width=0.01,color='y',align='center')
                specificity_bar = ax.bar(ind+4*width,specificities[0][i],width=0.01,color='purple',align='center')
                #print(categories)
                #ax.set_xticks(np.arange(N))
                #ax.set_xticklabels(categories)
                #ax.set_xticklabels(['No_cancer','cancer_type_1','cancer_type_4','cancer_type_6','cancer_type_7','cancer_type_12'])
                #ax.set_xticklabels(label_list)
                ax.set_xlabel(label_list[i])
                
                i+=1
                j+=1
        #plt.grid()
        #ax=plt.subplot(6,6,36)
        fig.legend((precision_bar, accuracy_bar,f1_bar,recall_bar,specificity_bar), ('precision', 'accuracy','f1','sensitivity','specificity'),loc='upper right')
        plt.draw()
        plt.pause(0.001)
        input("Press [enter] to Exit.")
                


        
if __name__ == '__main__':


        data_dir ="/Users/Arun/Documents/USC_Work/EE542/lab10/ee542_lab10/"

        data_file = data_dir + "miRNA_matrix.csv"
        
        df = pd.read_csv(data_file)
        df1=df.copy()
      
        y_data=df.pop('label').values
       
        df.pop('file_id')
      

        columns =df.columns
        #print (columns)
        X_data = df.values

        df1.pop('file_id')

        ##Visualization of data
        PCA_visual(X_data,df1)
        tsne_visual(X_data,df1)
        tsne_visual(X_data,df1,pca=True)


        # split the data to train and test set
        X_train, X_test, y_train, y_test = train_test_split(X_data, y_data, test_size=0.3, random_state=0)
        
        
        #standardize the data.
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

        

        #check the distribution of tumor and normal sampels in traing and test data set.
        #logging.info("Percentage of cancer cases in training set is {}".format(sum(y_train)/len(y_train)))
        #logging.info("Percentage of cancer cases in test set is {}".format(sum(y_test)/len(y_test)))
        
        n = 7
        feaures_columns = lassoSelection(X_train, y_train, n)



        scores = model_fit_predict(X_train[:,feaures_columns],X_test[:,feaures_columns],y_train,y_test)

        draw(scores)
  



 




